﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADO.NetDemo
{
    public class CompanyInfoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
