﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace ADO.NetDemo
{
    public class CompanyRepository
    {
        private readonly string _connectionString;

        public CompanyRepository(string connectionString)
            => _connectionString = connectionString;
        
        public bool DoesCompanyExist(string name)
        {
            using var connection = new SqlConnection(_connectionString);
            var command = connection.CreateCommand();
            
            connection.Open();
            command.CommandText = @$"SELECT COUNT(Id) FROM Companies WHERE Name = {name}";

            //command.CommandText = @$"SELECT COUNT(Id) FROM Companies WHERE Name = @Name";
            //command.Parameters.AddWithValue("@Name", name);

            var result = (int)command.ExecuteScalar();            

            return result > default(int);
        }

        public IEnumerable<Company> GetAll()
        {
            var resultSet = new List<Company>();

            using var connection = new SqlConnection(_connectionString);
            var command = connection.CreateCommand();

            connection.Open();
            command.CommandText = @"SELECT * FROM Companies";
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    resultSet.Add(new Company
                    {
                        Id = (int)reader["Id"],
                        Name = (string)reader["Name"]
                    });
                }
            }

            return resultSet;
        }

        public void Insert(Company company)
        {
            using var connection = new SqlConnection(_connectionString);

            var command = connection.CreateCommand();
            command.CommandText = @"INSERT INTO Companies (Name) VALUES (@Name)";

            var parameterName = command.CreateParameter();
            parameterName.ParameterName = "@Name";
            parameterName.Value = company.Name;
            command.Parameters.Add(parameterName);

            connection.Open();
            command.ExecuteNonQuery();
        }
    }
}