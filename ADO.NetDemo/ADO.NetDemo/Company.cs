﻿namespace ADO.NetDemo
{
    public class Company
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return $"Company: {Name} with Id: {Id}";
        }
    }
}
