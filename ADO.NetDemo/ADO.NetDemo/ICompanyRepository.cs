﻿using System.Collections.Generic;

namespace ADO.NetDemo
{
    public interface ICompanyRepository
    {
        void Insert(Company company);
        IEnumerable<Company> GetAll();
    }
}
