﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ADO.NetDemo
{
    class Program
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=AdoNetTraining;Integrated Security=True;";

        static void Main(string[] args)
        {
            var testCompany = new Company { Id = 1, Name = $"CompanyTest" };

            #region Automapper
            //InitializeMapper();
            //var dto = Mapper.Map<CompanyInfoDto>(testCompany);
            //var company = Mapper.Map<Company>(dto);
            #endregion

            #region Insert
            //Insert(testCompany);
            #endregion

            #region GetAll
            //var companies = GetAll();

            //foreach (var company in companies)
            //{
            //    Console.WriteLine(company);
            //}
            #endregion

            #region Repository pattern
            //var repo = new CompanyRepository(_connectionString);
            //repo.Insert(new Company
            //{
            //    Name = "CompanyFromRepo"
            //});

            //var companies = repo.GetAll();
            //foreach (var company in companies)
            //{
            //    Console.WriteLine(company);
            //}
            #endregion

            #region SQL Injection
            //var repo = new CompanyRepository(_connectionString);
            //var companyName = "CompanyFromRepo1651984351689465146987";
            //var doesCompanyExist = repo.DoesCompanyExist($"{companyName}' OR 1=1 --");
            //Console.WriteLine(doesCompanyExist ? $"You have successfully logged in with {companyName}" : "Wrong company name");
            #endregion
        }

        public static IEnumerable<Company> GetAll()
        {
            var resultSet = new List<Company>();
            using var connection = new SqlConnection(_connectionString);
            var command = connection.CreateCommand();

            connection.Open();
            command.CommandText = @"SELECT * FROM Companies";
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    resultSet.Add(new Company
                    {
                        Id = (int)reader["Id"],
                        Name = (string)reader["Name"]
                    });
                }
            }

            return resultSet;
        }

        public static void Insert(Company company)
        {
            // Create connection to DB
            using var connection = new SqlConnection(_connectionString);

            // Create command 
            var command = connection.CreateCommand();
            command.CommandText = @"INSERT INTO Companies (Name) VALUES (@Name)";

            var parameterName = command.CreateParameter();
            parameterName.ParameterName = "@Name";
            parameterName.Value = company.Name;
            command.Parameters.Add(parameterName);

            connection.Open();
            command.ExecuteNonQuery();
        }

        [Obsolete]
        public static void InitializeMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Company, CompanyInfoDto>();
            });
        }
    }
}