﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Extensions;
using RepositoryWithReflection.Repositories;
using RepositoryWithReflection.Services;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace RepositoryWithReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Reflection and Generics
            //ReflectionDemo<Company>();
            #endregion

            #region BaseRepository
            //CompanyRepository companyRepo = new CompanyRepository(Settings.DefaultConnection);
            //EmployeeRepository employeeRepo = new EmployeeRepository(Settings.DefaultConnection);

            //int companyId = companyRepo.Insert(new Company
            //{
            //    Name = "MI7",
            //    Address = "Not Classified"
            //});

            //employeeRepo.Insert(new Employee
            //{
            //    FirstName = "Pesho",
            //    LastName = "Peshev",
            //    CompanyId = companyId
            //});

            //IEnumerable<Employee> employees = employeeRepo.GetAll();
            //IEnumerable<Company> companies = companyRepo.GetAll();

            //foreach (var company in companies)
            //{
            //    Console.WriteLine(company);
            //}

            //foreach (var employee in employees)
            //{
            //    Console.WriteLine(employee);
            //}
            #endregion

            #region Business layer
            CompanyRepository companyRepo = new CompanyRepository(Settings.DefaultConnection);
            EmployeeRepository employeeRepo = new EmployeeRepository(Settings.DefaultConnection);
            EmployeeService employeeService = new EmployeeService(employeeRepo);
            CompanyService companyService = new CompanyService(companyRepo, employeeService);

            Company company = companyService.GetById(20);

            Console.WriteLine(company);

            foreach (var empl in company.Employees)
            {
                Console.WriteLine(empl);
            }

            #endregion
        }

        public static void ReflectionDemo<T>()
            where T : IBaseEntity, new()
        {
            Type type = typeof(T);
            Console.WriteLine($"Generic parameter is of type: {type.Name}.");
            Console.WriteLine($"It has the following properties: ");

            PropertyInfo[] properties = type.GetProperties();
            foreach (var prop in properties)
            {
                Console.WriteLine($"{prop.Name} of type {prop.PropertyType}.");
            }

            T obj = new T();

            Console.WriteLine();
            Console.WriteLine($"The current values are: ");
            foreach (var prop in properties)
            {
                object value = prop.GetValue(obj);
                Console.WriteLine($"{prop.Name} has value: {value}");
            }

            
            foreach (var prop in properties)
            {
                prop.SetValue(obj, Convert.ChangeType("1", prop.PropertyType));
            }
            obj.Id = 3;

            Console.WriteLine();
            Console.WriteLine($"The new values are: ");
            foreach (var prop in properties)
            {
                object value = prop.GetValue(obj);
                Console.WriteLine($"{prop.Name} has value: {value}");
            }

            Console.WriteLine();
            Console.WriteLine("Description of properties");
            foreach (var prop in properties)
            {
                string description = prop.GetDescription();
                Console.WriteLine($"{prop.Name} has description {description}.");
            }
        }
    }
}