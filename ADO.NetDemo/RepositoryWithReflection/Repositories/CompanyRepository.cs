﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Repositories.Interfaces;
using System.Data.SqlClient;

namespace RepositoryWithReflection.Repositories
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(string connectionstring)
            : base(connectionstring) { }

        //public IEnumerable<Company> GetAll()
        //{
        //    var resultSet = new List<Company>();

        //    using var connection = new SqlConnection(_connectionstring);
        //    var command = connection.CreateCommand();

        //    connection.Open();
        //    command.CommandText = @"SELECT * FROM Companies";
        //    using (var reader = command.ExecuteReader())
        //    {
        //        while (reader.Read())
        //        {
        //            resultSet.Add(new Company
        //            {
        //                Id = (int)reader["Id"],
        //                Name = (string)reader["Name"],
        //                Address = (string)reader["Address"]
        //            });
        //        }
        //    }
        //    return resultSet;
        //}

        //public int Insert(Company company)
        //{
        //    using SqlConnection connection = new SqlConnection(_connectionstring);

        //    SqlCommand command = connection.CreateCommand();
        //    command.CommandText = @"INSERT INTO Companies (Name, Address) OUTPUT INSERTED.ID VALUES (@Name, @Address)";

        //    SqlParameter parameterName = command.CreateParameter();
        //    parameterName.ParameterName = "@Name";
        //    parameterName.Value = company.Name;
        //    command.Parameters.Add(parameterName);

        //    SqlParameter parameterAddress = command.CreateParameter();
        //    parameterAddress.ParameterName = "@Address";
        //    parameterAddress.Value = company.Address;
        //    command.Parameters.Add(parameterAddress);

        //    connection.Open();
        //    return (int)command.ExecuteScalar();
        //}
    }
}
