﻿using RepositoryWithReflection.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryWithReflection.Repositories.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {
        int Insert(Company entity);
    }
}
