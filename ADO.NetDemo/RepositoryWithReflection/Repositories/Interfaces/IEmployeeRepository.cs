﻿using RepositoryWithReflection.Entities;
using System.Collections.Generic;

namespace RepositoryWithReflection.Repositories.Interfaces
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        IEnumerable<Employee> GetEmployeesForCompany(int companyId);
        int Insert(Employee entity);
    }
}