﻿using RepositoryWithReflection.Entities;
using System.Collections.Generic;

namespace RepositoryWithReflection.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity>
        where TEntity : IBaseEntity
    {
        TEntity GetById(int id);
        int Insert(TEntity entity);
        IEnumerable<TEntity> GetAll();
    }
}
