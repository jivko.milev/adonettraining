﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Pluralize.NET.Core;
using RepositoryWithReflection.Attributes;
using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Extensions;
using RepositoryWithReflection.Repositories.Interfaces;

namespace RepositoryWithReflection.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity, new()
    {
        protected readonly string _connectionstring;
        protected readonly Pluralizer _pluralizer;

        protected BaseRepository(string connectionstring)
        {
            _pluralizer = new Pluralizer();
            _connectionstring = connectionstring;
        }

        public virtual int Insert(TEntity entity)
        {
            using SqlConnection connection = new SqlConnection(_connectionstring);
            Type entityType = typeof(TEntity);

            string tableName = _pluralizer.Pluralize(entityType.Name);
            IEnumerable<string> columns = entityType
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotPopulate>() && !prop.HasAttribute<DoNotInsert>())
                .Select(prop => prop.Name);

            string columnsQueryVariable = string.Join(",", columns); //Name,Address,etc.

            IEnumerable<string> values = columns.Select(col => $"@{col}");
            string valuesQueryVariable = string.Join(",", values);

            string query = 
                $@"INSERT INTO {tableName} ({columnsQueryVariable}) OUTPUT INSERTED.ID VALUES ({valuesQueryVariable})";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            var parameters = GetParameters(entity);
            command.Parameters.AddRange(parameters);

            connection.Open();
            return (int)command.ExecuteScalar();
        }

        protected SqlParameter[] GetParameters(TEntity entity)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotPopulate>() && !prop.HasAttribute<DoNotInsert>());

            List<SqlParameter> parameters = new List<SqlParameter>();

            foreach (var prop in properties)
            {
                SqlParameter param = new SqlParameter();
                param.Value = prop.GetValue(entity);
                param.ParameterName = $"@{prop.Name}";
                parameters.Add(param);
            }

            return parameters.ToArray();
        }

        public virtual TEntity GetById(int id)
        {
            using SqlConnection connection = new SqlConnection(_connectionstring);
            Type entityType = typeof(TEntity);
            string tableName = _pluralizer.Pluralize(entityType.Name);

            string query = $"SELECT * FROM {tableName} WHERE Id = @Id";
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            SqlParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = id;
            command.Parameters.Add(parameter);

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                if (!reader.Read())
                    return default;

                TEntity entity = new TEntity();
                PopulateEntity(entity, reader);
                return entity;
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            using SqlConnection connection = new SqlConnection(_connectionstring);
            Type entityType = typeof(TEntity);

            string tableName = _pluralizer.Pluralize(entityType.Name);
            string query = $"SELECT * FROM {tableName}";

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            List<TEntity> result = new List<TEntity>();
            PropertyInfo[] properties = entityType.GetProperties();

            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    TEntity entity = new TEntity();
                    PopulateEntity(entity, reader);
                    result.Add(entity);
                }
            }

            return result;
        }

        /// <summary>
        /// Populates all of the entity properties
        /// </summary>
        /// <param name="entity">Entity to populate</param>
        /// <param name="reader">Reader from which to get the data</param>
        protected virtual void PopulateEntity(TEntity entity, IDataReader reader)
        {
            IEnumerable<PropertyInfo> properties = typeof(TEntity)
                .GetProperties()
                .Where(prop => !prop.HasAttribute<DoNotPopulate>());

            foreach (var property in properties)
            {
                object value = reader[property.Name];

                // SetValue convert null to the default value of the property type
                // This can also be done with reader.IsDBNull();
                if (value.GetType() == typeof(DBNull))
                    value = null;

                property.SetValue(entity, value);
            }
        }
    }
}