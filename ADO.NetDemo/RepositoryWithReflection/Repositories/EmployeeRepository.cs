﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RepositoryWithReflection.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(string connectionstring)
            : base(connectionstring) { }

        public IEnumerable<Employee> GetEmployeesForCompany(int companyId)
        {
            if (companyId == default)
                throw new ArgumentNullException();

            using SqlConnection connection = new SqlConnection(_connectionstring);
            var command = connection.CreateCommand();
            command.CommandText = $"SELECT * FROM Employees WHERE CompanyId = @CompanyId";

            SqlParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@CompanyId";
            parameter.Value = companyId;
            command.Parameters.Add(parameter);

            List<Employee> employees = new List<Employee>();
            connection.Open();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Employee empl = new Employee();
                    PopulateEntity(empl, reader);
                    employees.Add(empl);
                }
            }

            return employees;
        }

        //public IEnumerable<Employee> GetAll()
        //{
        //    var resultSet = new List<Employee>();

        //    using var connection = new SqlConnection(_connectionstring);
        //    var command = connection.CreateCommand();

        //    connection.Open();
        //    command.CommandText = @"SELECT * FROM Employees";
        //    using (var reader = command.ExecuteReader())
        //    {
        //        while (reader.Read())
        //        {
        //            resultSet.Add(new Employee
        //            {
        //                Id = (int)reader["Id"],
        //                FirstName = (string)reader["FirstName"],
        //                LastName = (string)reader["LastName"]
        //            });
        //        }
        //    }

        //    return resultSet;
        //}

        //public int Insert(Employee employee)
        //{
        //    using SqlConnection connection = new SqlConnection(_connectionstring);

        //    SqlCommand command = connection.CreateCommand();
        //    command.CommandText = @"INSERT INTO Employees (FirstName, LastName) OUTPUT INSERTED.ID VALUES (@FirstName, @LastName)";

        //    SqlParameter parameterFirstName = command.CreateParameter();
        //    parameterFirstName.ParameterName = "@FirstName";
        //    parameterFirstName.Value = employee.FirstName;
        //    command.Parameters.Add(parameterFirstName);

        //    SqlParameter parameterLastName = command.CreateParameter();
        //    parameterLastName.ParameterName = "@LastName";
        //    parameterLastName.Value = employee.LastName;
        //    command.Parameters.Add(parameterLastName);

        //    connection.Open();
        //    return (int)command.ExecuteScalar();
        //}
    }
}