﻿using RepositoryWithReflection.Attributes;

namespace RepositoryWithReflection.Entities
{
    public abstract class BaseEntity : IBaseEntity
    {
        [DoNotInsert]
        public int Id { get; set; }
    }
}
