﻿using RepositoryWithReflection.Attributes;
using System.Collections.Generic;
using System.ComponentModel;

namespace RepositoryWithReflection.Entities
{
    [Description("ReflectionDemo")]
    public class Company : BaseEntity
    {
        public string Name { get; set; }

        [Description("Company Address")]
        public string Address { get; set; }

        [DoNotPopulate]
        public IEnumerable<Employee> Employees { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Company: {Name} is located at {Address}.";
        }
    }
}
