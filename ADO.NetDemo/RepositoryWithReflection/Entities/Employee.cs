﻿using RepositoryWithReflection.Attributes;

namespace RepositoryWithReflection.Entities
{
    public class Employee : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int CompanyId { get; set; }

        [DoNotPopulate]
        public Company Company { get; set; }

        public override string ToString()
        {
            return $"My name is {LastName}, {FirstName} {LastName} with CompanyId {CompanyId}.(Id: {Id})";
        }
    }
}
