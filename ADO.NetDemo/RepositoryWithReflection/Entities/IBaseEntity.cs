﻿namespace RepositoryWithReflection.Entities
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
