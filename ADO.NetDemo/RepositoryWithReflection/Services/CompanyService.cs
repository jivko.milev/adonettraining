﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryWithReflection.Services
{
    public class CompanyService : BaseService<Company, CompanyRepository>
    {
        private readonly EmployeeService _employeeService;

        public CompanyService(CompanyRepository repository,
            EmployeeService employeeService) 
            : base(repository)
        {
            _employeeService = employeeService;
        }

        public override Company GetById(int id)
        {
            Company company = base.GetById(id);

            company.Employees = _employeeService.GetEmployeesForCompany(company.Id);
            return company;
        }
    }
}
