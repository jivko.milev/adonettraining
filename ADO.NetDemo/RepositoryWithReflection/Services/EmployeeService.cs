﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryWithReflection.Services
{
    public class EmployeeService : BaseService<Employee, EmployeeRepository>
    {
        public EmployeeService(EmployeeRepository repository) 
            : base(repository) { }

        public IEnumerable<Employee> GetEmployeesForCompany(int companyId)
        {
            if (companyId == 0)
            {
                throw new ArgumentException();
            }

            IEnumerable<Employee> employees = _repository.GetEmployeesForCompany(companyId);

            return employees;
        }
    }
}
