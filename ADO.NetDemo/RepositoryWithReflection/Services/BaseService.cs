﻿using RepositoryWithReflection.Entities;
using RepositoryWithReflection.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryWithReflection.Services
{
    public abstract class BaseService<TEntity, TRepository>
        where TEntity : IBaseEntity
        where TRepository : IBaseRepository<TEntity>
    {
        protected readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<TEntity> GetAll()
        {
            var entities = _repository.GetAll();

            return entities;
        }

        public virtual TEntity GetById(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("The id must be a positive integer number.");
            }

            TEntity entity = _repository.GetById(id);

            return entity;
        }

        public int Insert(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The Entity must not be null.");
            }

            return _repository.Insert(entity);
        }

    }
}
