﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace RepositoryWithReflection.Extensions
{
    public static class Extensions
    {
        public static bool HasAttribute<AttributeType>(this PropertyInfo prop)
            where AttributeType : Attribute
        {
            var attributes = prop.GetCustomAttributes(typeof(AttributeType));

            return attributes.Count() > 0;
        }

        public static string GetDescription(this PropertyInfo prop)
        {
            var descriptionAttribute = prop.GetCustomAttribute<DescriptionAttribute>();
            if (descriptionAttribute == default)
            {
                return string.Empty;
            }

            return descriptionAttribute.Description;
        }
    }
}
