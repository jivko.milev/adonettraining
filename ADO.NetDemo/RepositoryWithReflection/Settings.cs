﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace RepositoryWithReflection
{
    public static class Settings
    {
        private static IConfiguration config;

        static Settings()
        {
            config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public static string DefaultConnection
        {
            get
            {
                return config.GetConnectionString("DefaultConnection");
            }
        }

        public static string XmlFilePath
        {
            get
            {
                return config.GetSection("FilePaths")["XmlFilePath"];
            }
        }
    }
}
